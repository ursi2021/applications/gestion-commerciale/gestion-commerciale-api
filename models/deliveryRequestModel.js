const { DataTypes } = require('sequelize');
const db = require("./sequelize"),
    sequelize = db.sequelize,
    Sequelize = db.Sequelize;
const logger = require("../logs");

const insertJSON = require('./json/delivery_request.json')

/**
 * @swagger
 * definitions:
 *  deliveryRequest:
 *      type: object
 *      properties:
 *          provider_id:
 *              type: int
 *          product_id:
 *              type: int
 *          quantity:
 *              type: int
 *          date:
 *              type: date
 *          delivery_date:
 *              type: date

 */
class deliveryRequest extends Sequelize.Model  {}
deliveryRequest.init(
    {
        destinationId: {
            type: DataTypes.STRING,
        },
        date: {
            type: DataTypes.DATE
        },
        deliveryDate: {
            type: DataTypes.DATE
        }
    },
    {
        sequelize,
        ModelName: "deliveryRequest",
        timestamps: false
        // Other model options go here
    }
)

deliveryRequest.sync();
logger.info("The table for the deliveryRequest model was just (re)created!");

/*deliveryRequest.findAll().then(prod => {
    if (prod.length === 0) {
        const cody = deliveryRequest.bulkCreate(insertJSON).then(res => {
            //FIX
        }).catch(function (e) {
            console.log(e)
        })
    }
}).catch(function (e) {
    console.log(e)
})*/

module.exports = deliveryRequest

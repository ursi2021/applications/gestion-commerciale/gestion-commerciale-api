const logger = require('../logs');
const { Sequelize } = require('sequelize');

const  sequelize = require('../services/connectDBServices')

let db = {};
db.sequelize = sequelize;
db.Sequelize = Sequelize;
module.exports = db;

const { DataTypes } = require('sequelize');
const db = require("./sequelize"),
    sequelize = db.sequelize,
    Sequelize = db.Sequelize;
const logger = require("../logs");
const insert = require('./json/provider_orders.json')
const insertProduct = require('./json/product_quantity.json')

/**
 * @swagger
 * definitions:
 *  providerOrders:
 *      type: object
 *      properties:
 *          provider_id:
 *              type: int
 *          product_id:
 *              type: int
 *          invoice:
 *              type: int
 *          order_number:
 *              type: int
 *          delivery_date:
 *              type: date

 */
class providerOrders extends Sequelize.Model {}
providerOrders.init(
    {
        providerId: {
            type: DataTypes.INTEGER
        },
        invoiceId: {
            type: DataTypes.INTEGER
        },
        orderId: {
            type: DataTypes.INTEGER
        },
        deliveryDate: {
            type: DataTypes.DATE
        },
    },
    {
        sequelize,
        ModelName: "providerOrders",
        timestamps: false
        // Other model options go here
    }
);

//providerOrders.hasMany(productOrders)
providerOrders.sync();
logger.info("The table for the providerOrders model was just (re)created!");

/*providerOrders.findAll().then(prod => {
    if (prod.length === 0) {
        const cody = providerOrders.bulkCreate(insert).then(res => {
            //FIX

        }).catch(function (e) {
            console.log(e)
        })
    }
}).catch(function (e) {
    console.log(e)
})*/

module.exports = providerOrders



const { DataTypes } = require('sequelize');
const db = require("./sequelize"),
    sequelize = db.sequelize,
    Sequelize = db.Sequelize;
const logger = require("../logs");
const insertProduct = require('./json/product_quantity.json')

/**
 * @swagger
 * definitions:
 *  productQuantity:
 *      type: object
 *      properties:
 *          product_code:
 *              type: int
 *          quantity:
 *              type: int
 *

 */
class productQuantity extends Sequelize.Model {}
productQuantity.init(
    {
        productCode: {
            type: DataTypes.STRING,
        },
        quantity: {
            type: DataTypes.INTEGER
        },
        providerOrderId : {
        	type: DataTypes.INTEGER
        },
        deliveryRequestId : {
            type: DataTypes.INTEGER
        }
    },
    {
        sequelize,
        ModelName: "productQuantity",
        timestamps: false
        // Other model options go here
    }
);
productQuantity.sync();
logger.info("The table for the productQuantity model was just (re)created!");

/*productQuantity.findAll().then(prod => {
    if (prod.length === 0) {
        const cody = productQuantity.bulkCreate(insertProduct).then(res => {
            //FIX

        }).catch(function (e) {
            console.log(e)
        })
    }
}).catch(function (e) {
    console.log(e)
})*/


module.exports = productQuantity


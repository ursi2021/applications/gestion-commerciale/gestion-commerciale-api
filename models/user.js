const { DataTypes } = require('sequelize');
const db = require("./sequelize"),
	sequelize = db.sequelize,
	Sequelize = db.Sequelize;
const logger = require("../logs");


/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      properties:
 *          firstName:
 *              type: string
 *          lastName:
 *              type: string
 */
class User extends Sequelize.Model {}
User.init(
    {
        firstName: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        lastName: {
            type: Sequelize.DataTypes.STRING
            // allowNull defaults to true
        },
    },
    {
        sequelize,
        ModelName: "user",
        timestamps: false
        // Other model options go here
    }
);

User.sync();
logger.info("The table for the User model was just (re)created!");

module.exports = User;

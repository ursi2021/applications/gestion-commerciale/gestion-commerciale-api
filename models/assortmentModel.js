const db = require("./sequelize"),
	sequelize = db.sequelize,
	Sequelize = db.Sequelize;
const logger = require("../logs");
const insertJSON = require('./json/assortment.json')
const productServices =  require('../services/productsServices')

const json = [
];


async function test() {
	const result = await productServices.getProducts()

	for (var i = 0; i < result.length; i++) {
		elm = {
			"product_code": -1,
			"assortment_type": "",
		};

		rand = Math.floor(Math.random() * Math.floor(3));
		if (rand === 1){
			elm["product_code"] = result[i]["product-code"]
			elm["assortment_type"] = "Web";
		}
		else if (rand === 2){
			elm["product_code"] = result[i]["product-code"]
			elm["assortment_type"] = "Store";
		}
		else{
			elm["product_code"] = result[i]["product-code"]
			elm["assortment_type"] = "Both";
		}
		json.push(elm);
	}

	Assortments.sync();
	logger.info("The table for the Assortment model was just (re)created!");
	Assortments.findAll().then(prod => {
		if (prod.length === 0) {
			const cody = Assortments.bulkCreate(json).then(res => {
				//FIX
			}).catch(function (e) {
				console.log(e)
			})
		}
	}).catch(function (e) {
		console.log(e)
	})
};

test();



/**
 * @swagger
 * definitions:
 *  Assortment:
 *      type: object
 *      properties:
 *          product_cpde:
 *              type: string
 *          assortment_type:
 *              type: string
 */
class Assortments extends Sequelize.Model {}
Assortments.init(
    {
        product_code: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        assortment_type: {
            type: Sequelize.DataTypes.STRING
            // allowNull defaults to true
        },
    },
    {
        sequelize,
        ModelName: "Assortments",
        timestamps: false
        // Other model options go here
    }
);

module.exports = Assortments

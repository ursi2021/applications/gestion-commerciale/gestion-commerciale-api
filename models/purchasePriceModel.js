const { DataTypes } = require('sequelize');
const db = require("./sequelize"),
    sequelize = db.sequelize,
    Sequelize = db.Sequelize;
const logger = require("../logs");
//const json = require("../models/json/purchase_price_demo.json")

/**
 * @swagger
 * definitions:
 *  providerOrders:
 *      type: object
 *      properties:
 *          provider_id:
 *              type: int
 *          product_id:
 *              type: int
 *          invoice:
 *              type: int
 *          order_number:
 *              type: int
 *          delivery_date:
 *              type: date

 */
class purchasePrices extends Sequelize.Model {}
purchasePrices.init(
    {
        productCode: {
            type: DataTypes.STRING
        },
        productFamily: {
            type: DataTypes.STRING
        },
        purchasePrice: {
            type: DataTypes.INTEGER
        }
    },
    {
        sequelize,
        ModelName: "purchasePrices",
        timestamps: false
        // Other model options go here
    }
);

//providerOrders.hasMany(productOrders)
purchasePrices.sync();
logger.info("The table for the purchasePrices model was just (re)created!");

purchasePrices.findAll().then(prod => {
    if (prod.length === 0) {
        /*const cody = purchasePrices.bulkCreate(json).then(res => {
            //FIX
        }).catch(function (e) {
            console.log(e)
        })*/
    }
}).catch(function (e) {
    console.log(e)
})

module.exports = purchasePrices



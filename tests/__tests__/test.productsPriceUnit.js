const axios = require('axios');

jest.mock('axios');

jest.mock('../../services/connectDBServices', () => {
    const Sequelize = require('sequelize');
    const sequelize = new Sequelize('database', 'username', 'password', {
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false
    });
    sequelize.authenticate();
    return sequelize;
});

const productPrice = require('../../models/purchasePriceModel')
const productPriceService = require('../../services/productPricesServices')

describe ('Test lul', () => {
    test('Define product', async () => {
        const res = {data: [{
            "id": 1,
            "product-code": "X1-0",
            "product-family": "Frigos",
            "product-description": "Frigos:P1-0",
            "min-quantity": 15,
            "packaging": 2,
            "price": 165.3,
            "assortment-type": "Store"},
            {
                "id": 2,
                "product-code": "X1-1",
                "product-family": "Console",
                "product-description": "Console:P3-1",
                "min-quantity": 5,
                "packaging": 1,
                "price": 324.72,
                "assortment-type": "Both"},
                {
                    "id": 3,
                    "product-code": "X1-2",
                    "product-family": "Frigos",
                    "product-description": "Frigos:P3-2",
                    "min-quantity": 10,
                    "packaging": 1,
                    "price": 57,
                    "assortment-type": "Both"}]}
        axios.get.mockResolvedValue(res)
        await productPriceService.definePurchasePrices()
        return productPriceService.definePurchasePrices().then(data => expect(data.length).toEqual(res.data.length))
      });
})

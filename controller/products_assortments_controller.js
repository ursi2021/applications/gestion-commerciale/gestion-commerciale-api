let express = require('express');
let app = express();

let controller = {}

module.exports = controller

const Assortment = require('../models/assortmentModel');

const productServices =  require('../services/productsServices')

controller.getProducts = async function(req, res, next) {
    const result = await productServices.getProducts()
    res.render('products', {title: 'Produits', products: result});
}

function renameField(products) {
    const result= []
    products.map(product => {
        const elt = {
            id: product.id,
            'product-code': product.product_code,
            'assortment-type': product.assortment_type
        }
        result.push(elt)
    })
    return result
}

controller.productAssortments = async function(req, res, next) {
    if (req.params.assortments !== "Web" && req.params.assortments !== "Store" && req.params.assortments !== 'Both'){
        res.status(400).json("Wrong assormtents paramaters, please use Web or Store or Both (and yep, this is case sensitive ;) )");
    };
    Assortment
        .findAll()
        .then(products => {
            const result = renameField(products)
            products = result.filter(function(item){
                return item["assortment-type"] === req.params.assortments;
            });
            res.status(200).json(products)})
        .catch(err => res.status(404).json({error : 'Cannot connect to DB'}))
}

controller.allProductAssortments = async function(req, res, next)
{
    Assortment
        .findAll()
        .then(assortments => {
            const result = renameField(assortments)
            res.json(result)
        })
        .catch(err => res.status(404).json({error : 'Cannot connect to DB'}))
}

controller.productsTraitment = async function(req, res, next) {
    const products = req.body

    for (var i = 0; i < products.length; i++) {
        products[i]["purchase-price"] = Math.floor(Math.random() * Math.floor(300))

		rand = Math.floor(Math.random() * Math.floor(3));
		if (rand === 1){
            products[i]["assortment-type"] = "Web";
		}
		else if (rand === 2){
			products[i]["assortment-type"] = "Store";
		}
		else{
			products[i]["assortment-type"] = "Both";
		}
    }

    const response = await productServices.postProductsAssortmentPrices(products)
    const val = response[0], code = response[1]
    if (code === 200)
        res.status(200).json(val);
    else
        res.status(404).json({error: val})

}

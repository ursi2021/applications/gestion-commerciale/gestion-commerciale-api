var controller = {}, _app = {};
var User = require('../models/user');
let logger = require('../logs');

module.exports = controller

controller.index = function(req, res, next) {
    res.render('index', { title: 'Express' });
}

controller.getClientsPage = function(req, res, next) {
    //
    User.findAll().then(function(users) {
        logger.info(users);
        res.render('users', {title: 'Users', users: users});
    });
}

controller.getClients = function(req, res, next) {
    try {
        User.findAll().then(user => res.status(200).json(user));
    } catch (e) {
        res.status(404).json({'error': 'FindAll'});
    }
}

controller.postClients = function(req, res, next) {
    try {
        User.create(req.body).then(function (user) {
            res.status(200).json(user);
        });
    } catch (e) {
        res.status(404);
    }
}

let express = require('express');
let app = express();
let controller = {};

module.exports = controller

const storeRestockingServices = require('../services/storeRestockServices')
const warehouseServices = require('../services/warehouseServices')
const replenishmentServices = require('../services/replenishementServices')

controller.warehouseStockPosition = async function(req, res, next) {
    const result = await warehouseServices.getStockPosition()
    res.status(200).json(result);
}

controller.replenisment =  async function(req, res, next) {
    const stock_result = await replenishmentServices.getDeliveryRequests()
    res.status(200).json(stock_result);
}

controller.caluclReplenisment = async function(req, res, next) {
    return replenishmentServices.caluclReplenisment(req, res, next)
}

controller.commandWarehouseReapro = async function(req, res, next) {
    return replenishmentServices.commandWarehouseReapro(req, res, next)
}

controller.storeRestockingRequest = async function(req, res, next) {
    const store_restocking_requests = await storeRestockingServices.getRestockingRequest(req, res, next)
    res.status(200).json(store_restocking_requests);
}

controller.clockCommands = async function(req, res, next) {
	const clock_commands = await replenishmentServices.clockCommands(req, res, next)
    res.status(200).json(clock_commands)
}

let express = require('express');
let router = express.Router();
let logger = require('../logs');

let app = express();

router.get("", async (req, res, next) => { 
	try { 
		const jobs = [ // List of Treatments
			{ 	
				j: 0, // 0 is everyday
				h: 21, // 21h 
				m: 0, // 0 min
				date: null, // null because j = 0 (Recurrent Treatment) 
				name: "Get sales every day at 21h", // Treatment's name 
				route: "/gestion-commerciale/clock-sales", // POST will be done on this route
				// params will be sent in the POST body
			}, 
			{ 	
				j: 0, // 0 is everyday
				h: 21, // 21h 
				m: 20, // 0 min
				date: null, // null because j = 0 (Recurrent Treatment) 
				name: "Get commands every day at noon", // Treatment's name 
				route: "/gestion-commerciale/clock-commands", // POST will be done on this route
				// params will be sent in the POST body
			}, 
		]; 
		res.status(200).json(jobs);
	} catch (error) { 
		logger.error(error); res.sendStatus(404); 
	} 
});

module.exports = router;

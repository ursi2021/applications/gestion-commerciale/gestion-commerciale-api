let controller = {}

module.exports = controller

const Assortment = require('../models/assortmentModel');

const productServices =  require('../services/productsServices')
const clientsServices =  require('../services/clientsServices')

controller.index = function(req, res, next) {
    res.render('gestioncommerciale', { title: 'Gestion commerciale' });
}

controller.gestco_page1 = function(req, res, next) {
    res.render('gestco/gestco_page1', {title: 'Gestion commerciale page 1'});
}

controller.gestco_page2 = function(req, res, next) {
    res.render('gestco/gestco_page2', {title: 'Gestion commerciale page 2'});
}

controller.getClientsRefPage = async function(req, res, next) {
    const result = await clientsServices.getClients()
    res.render('clients', {title: 'Clients', clients: result});
}

controller.getClientsRefData = async function(req, res, next) {
    const result = await clientsServices.getClients()
    res.status(200).json(result)
}

controller.getProductsRefData = async function(req, res, next) {
    const result = await productServices.getProducts()
    res.status(200).json(result);
}

controller.getProductsTest = async function(req, res, next) {
    const result = await productServices.getProducts()
    console.log(result);
    Assortment.bulkCreate(json({result})).then(res => {
        //FIX
    }).catch(function (e) {
        console.log(e)
    })
}

const helloServices =  require('../services/helloServices')

let controller = {}

module.exports = controller

controller.hello = function(req, res, next) {
    res.send({'gestion-commerciale': 'Hello World ! '});
}

controller.helloAll = async function (req, res, next) {
    const result = await helloServices.getHelloApps();
    res.render('hello', {hello: result});
}

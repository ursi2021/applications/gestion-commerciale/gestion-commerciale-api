var controller = {};

module.exports = controller

const salesServices =  require('../services/salesServices')

controller.webSales = async function (req, res, next) {
    const web_sales = await salesServices.webSales()
    res.status(200).json(web_sales)
}

controller.storeSales = async function (req, res, next) {
    const store_sales = await salesServices.storeSales()
    res.status(200).json(store_sales)
}

controller.allSales = async function(req, res, next) {
	const all_sales = await salesServices.allSales()
    res.status(200).json(all_sales)

}

controller.clockSales = async function(req, res, next) {
	const clock_sales = await salesServices.clockSales()
    res.status(200).json(clock_sales)
}
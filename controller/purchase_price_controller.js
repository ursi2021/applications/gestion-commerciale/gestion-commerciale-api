let controller = {}

module.exports = controller

const productPricesServices =  require('../services/productPricesServices')

controller.purchasePrice = async function(req, res, next) {
    const result = await productPricesServices.definePurchasePrices()
    res.status(200).json(result);
}

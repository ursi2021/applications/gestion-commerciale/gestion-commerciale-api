let express = require('express');
let app = express();
let logger = require('../logs');

let controller = {}

module.exports = controller

let deliveryRequest = require('../models/deliveryRequestModel');
let productOrder = require('../models/z_productOrdersModel');

controller.getProductMap = async function() {
    return await productOrder.findAll().then(products => {
        const result = products
        products = result.map(function(item){
            return item.dataValues;
        });
        return products
    })
}

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

controller.getDeliveryRequests = function(req, res, next) {
    deliveryRequest.findAll().then(async delivery_requests => {
        let products_map = await controller.getProductMap()
        let resul = []
        delivery_requests.forEach(delivery_request => {
            let product_res = {
                "product-quantity-map" : []
            }
            products_map.forEach(item => {
                if (item.deliveryRequestId === delivery_request.id) {
                    const tmp = {
                        "product-code": item.productCode,
                        "quantity": item.quantity
                    }
                    product_res["product-quantity-map"].push(tmp)
                }
            })

            const result = {
                "product-quantity-map": product_res["product-quantity-map"],
                "destination-id": delivery_request.destinationId,
                "date": delivery_request.date,
                "delivery-date": delivery_request.deliveryDate,
                "delivery-id": delivery_request.id 
            }
            resul.push(result)
            return result
        })
        res.status(200).json(resul)
    })
}

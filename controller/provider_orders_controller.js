let express = require('express');
let app = express();
let logger = require('../logs');

let controller = {}

module.exports = controller

let providerOrders = require('../models/providerOrdersModel');
let productOrder = require('../models/z_productOrdersModel');

controller.getProductMap = async function() {
    return await productOrder.findAll().then(products => {
        const result = products
        products = result.map(function(item){
            return item.dataValues;
        });
        return products
    })
}

controller.getProvideOrder = function (req, res, next) {
    providerOrders.findAll().then(async provider_orders => {
        let products_map = await controller.getProductMap()
        let resul = []
        provider_orders.forEach(provider_order => {
            const product_res = {
                "product-quantity-map" : []
            }
            products_map.forEach(item => {
                if (item.providerOrderId === provider_order.id) {
                    const tmp = {
                        "product-code": item.productCode,
                        "quantity": item.quantity
                    }
                    product_res["product-quantity-map"].push(tmp)
                }
            })
            const result = {
                "provider-id": provider_order.id,
                "product-quantity-map": product_res["product-quantity-map"],
                "invoice-id": provider_order.invoiceId,
                "order-id": provider_order.orderId,
                "delivery-date": provider_order.deliveryDate
            }
            resul.push(result)
            return result
        })
        res.status(200).json(resul)
    })

}

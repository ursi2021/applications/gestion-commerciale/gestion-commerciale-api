const axios = require("axios");

const purchasePrice = require('../models/purchasePriceModel')

async function definePurchasePrices() {
    let products_list = '';
    await axios.get(process.env.KONG_URL_REFERENTIEL_PRODUIT + '/list_products').then(function (products) {
        products_list = products.data;

    }).catch((err) => {
        products_list = err.data === undefined ? {"myerror": "My error : return data type /products is undefined: maybe table is empty"} : err.data;
    });
    products_list.map(async product_list => {
        const project = await purchasePrice.findOne({ where: { productCode: product_list["product-code"] } });
        if (project === null) {
            let item = {
                productCode: product_list["product-code"],
                productFamily: product_list["product-family"],
                purchasePrice: Math.floor(Math.random() * Math.floor(300))
            }
            purchasePrice.create(item)
        }
    })
    let result = []
    await purchasePrice.findAll().then(purchase_prices => {
        purchase_prices.map(purchase_price => {
            const res = {
                "id": purchase_price.dataValues.id,
                "product-code": purchase_price.dataValues.productCode,
                "product-family": purchase_price.dataValues.productFamily,
                "purchase-price": purchase_price.dataValues.purchasePrice
            }
            result.push(res)
        })
    })
    return result;
}

module.exports.definePurchasePrices = definePurchasePrices;

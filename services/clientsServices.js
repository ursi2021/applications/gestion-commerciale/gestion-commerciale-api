const axios = require("axios");

async function getClients() {
    let result = '';
    await axios.get(process.env.KONG_URL_RELATION_CLIENT + '/clients').then(function (clients) {
        result = clients.data;
    }).catch((err) => {
        result = err.data === undefined ? {"myerror": "My error : return data type /clients is undefined: maybe table is empty"} : err.data;
    });
    
    return result
}

module.exports.getClients = getClients;
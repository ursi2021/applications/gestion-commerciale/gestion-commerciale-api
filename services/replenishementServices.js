const axios = require("axios");

let providerOrders = require('../models/providerOrdersModel');
let productOrder = require('../models/z_productOrdersModel');
let deliveryRequest = require('../models/deliveryRequestModel');

async function createDelivery(req, res, next) {
    await deliveryRequest.create(req).then(order => {
        console.log("deliveryRequest created")
    })
}
async function createProviderOrder(req, res, next) {
    await providerOrders.create(req).then(order => {
        console.log("providerOrders created")
    })
}
 async function createProductOrder(req, res, next) {
    await productOrder.create(req).then(order => {
        console.log("productOrder created")
    })
}

Date.prototype.addDays = function(days) {
    let date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

async function getLastDelivery() {
    return await deliveryRequest.findAll({
        limit: 1,
        where: {},
        order: [ [ 'id', 'DESC' ]]
    }).then(function(entries){return entries});
}

async function getLastProvider() {
    return await providerOrders.findAll({
        limit: 1,
        where: {},
        order: [ [ 'id', 'DESC' ]]
    }).then(function(entries){return entries});
}

function checkDB(id, elt) {
    if (elt[0] !== undefined) {
        id = elt[0].dataValues.id + 1
    }
    return id
}

async function caluclReplenisment(req, res, next) {
    let nop = []
    // Get Store replenishment request
    let store_replenisment_request = []
    await axios.get(process.env.KONG_URL_BACK_OFFICE_MAGASIN + '/store-restocking-request')
        .then(function (res_reassort) {
            store_replenisment_request = res_reassort.data["product-quantity-map"]
            res.send('Replenishments from back office sent to providers')
        })
        .catch((err) => {
            res.status(404).json({error : 'Cannot connect to /store-restocking-request from Back office magasin'})
        })

    // Get min quantity for the requested products
    let product_min_quantity = []
    await axios.get(process.env.KONG_URL_REFERENTIEL_PRODUIT + '/list_products')
        .then(function (products) {
            store_replenisment_request.map(store_replenisment_product => {
                products.data.map(product => {
                    if (product["product-code"] === store_replenisment_product["product-code"]) {
                        const item = {
                            "product-code": store_replenisment_product["product-code"],
                            "quantity": store_replenisment_product["quantity"],
                            "min-quantity": product["min-quantity"]
                        }
                        nop.push(item)
                    }
                })
            })
        })
        .catch((err) => {
            res.status(404).json({error : 'Cannot connect to /list_products from products'})
        })
    //console.log(store_replenisment_request)
    // Get the stock of this product in warehouse
    let result = []
    await axios.get(process.env.KONG_URL_ENTREPROTS + '/warehouse-stock-position')
        .then(function (warehouse_stock) {
            nop.map(store_replenisment_product => {
                warehouse_stock.data["product-quantity-map"].map(product_stock => {
                    if (product_stock["product-code"] ===  store_replenisment_product["product-code"]) {
                        const item = {
                            "product-code": store_replenisment_product["product-code"],
                            "quantity": store_replenisment_product["quantity"],
                            "min-quantity": store_replenisment_product["min-quantity"],
                            "stock-quantity": product_stock["quantity"]
                        }
                        result.push(item)
                    }
                })
            })
        })
        .catch((err) => {
            res.status(404).json({error : 'Cannot connect to /warehouse-stock-position from stock'})
        })
    
    console.log(result)
    let del_id = 1, prov_id = 1
    let last_del = await getLastDelivery()
    let last_prov = await getLastProvider()

    let date = new Date()
    let yo = 0

    // Check if quantity in warehouse is enough
    // If enough => It's ok
    // If it's not enough => Launch a provider order with this product and receive a provider order availible for warehouse
    result.map(async products => {
        if (products["stock-quantity"] - products["quantity"] <= products["min-quantity"] ) {
            if (yo === 0) {
                yo ++
                const delivery = {
                    "destinationId": 1,
                    "date": date,
                    "deliveryDate": date.addDays(3)
                }
                const order = {
                    "providerId": 1,
                    "invoiceId": 1,
                    "orderId": 1,
                    "deliveryDate": date
                }
                await createDelivery(delivery)
                await createProviderOrder(order)
                console.log("commande envoyée aux fournisseurs")
            }
            del_id = checkDB(del_id, last_del)
            prov_id = checkDB(prov_id, last_prov)

            const product_order = {
                "productCode": products["product-code"],
                "quantity": products["quantity"] * 2,
                "deliveryRequestId": del_id,
                "providerOrderId": prov_id
            }
            await createProductOrder(product_order)
        }
    })
}

async function commandWarehouseReapro(req, res, next) {
    let del_id = 1, prov_id = 1

    let last_del = await getLastDelivery()
    del_id = checkDB(del_id, last_del)

    let last_prov = await getLastProvider()
    prov_id = checkDB(prov_id, last_prov)

    let date = new Date()
    await axios.get(process.env.KONG_URL_ENTREPROTS + '/reapprovisionnement').then(function (reapro) {
        reapro.data.map(async warehouse_reappro_product => {
        	//if (warehouse_reappro_product.date == date.getDay())
        	//{
            	const delivery = {
                	"destinationId": 1,
                	"date": date,
                	"deliveryDate": date.addDays(3)
            	}
            	const order = {
                	"providerId": 1,
                	"invoiceId": 1,
                	"orderId": 1,
                	"deliveryDate": date
                }
                await createDelivery(delivery)
                await createProviderOrder(order)
                
                console.log("commande envoyée aux fournisseurs")
                
                warehouse_reappro_product["product-quantity-map"].map(async product_stock => {
                    const item = {
                        "productCode": product_stock["product-code"],
                        "quantity": product_stock["quantity"],
                        "deliveryRequestId": del_id,
                        "providerOrderId": prov_id
                    }
                    await createProductOrder(item)
                })
            //}
        })
        res.status(200).send('Replenishments from warehouse sent to providers')
    })
    .catch((err) => {
        res.status(404).json({error : 'Cannot connect to /reapprovisionnement from Gestion-entrepôts'})
    })
}

async function getDeliveryRequests () {
    let stock_result = '';
    let replenishment = [];
    //let date = new Date()
    await axios.get(process.env.KONG_URL_GESTION_COMMERCIALE + '/delivery-requests').then(function (request) {
        //request.data.map(async delivery_product => {
            //if (delivery_product.date.getDate() == date.getDate())
            //{
            //replenishment.push(delivery_product)
            //}
        //})
        stock_result = request.data;
    }).catch((err) => {
        replenishment = err.data === undefined ? {"myerror": "My error : return data type /replenishment is undefined: maybe table is empty"} : err.data;
    });
    return stock_result
}

async function clockCommands(req, res, next) {
    const warehouse_replishment = await commandWarehouseReapro(req, res, next)
    //const store_replenishment = await commandWarehouseReapro()
    const store_replenishment = await caluclReplenisment(req, res, next)
    return true
}


module.exports.commandWarehouseReapro = commandWarehouseReapro
module.exports.caluclReplenisment = caluclReplenisment
module.exports.getDeliveryRequests = getDeliveryRequests
module.exports.clockCommands = clockCommands
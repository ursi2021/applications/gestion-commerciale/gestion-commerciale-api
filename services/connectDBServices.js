const logger = require('../logs')

const { Sequelize } = require('sequelize');

// login to mariadb database
// ex : new  new Sequelize(<dbname>, <user>, <password>, ...);
const sequelize = new Sequelize(
    process.env.URSI_DB_NAME || "gestionCommerciale",
    process.env.URSI_DB_USER || "root",
    process.env.URSI_DB_PASSWORD || "azerty",
    {
        host: process.env.URSI_DB_HOST || 'localhost',
        port: Number(process.env.URSI_DB_PORT || '3306'),
        logging: (msg) => logger.verbose(msg),
        dialectOptions: {
            timezone: "Etc/GMT0",
        },
        dialect: "mariadb",
    }
);

(async () => {
    try {
        await sequelize.authenticate();
        logger.info("Connection has been established successfully.");
    } catch (error) {
        logger.error("Unable to connect to the database:", error);
    }
})();


module.exports = sequelize;

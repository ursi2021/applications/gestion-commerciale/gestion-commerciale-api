const axios = require("axios");

async function getProducts() {
    let result = '';
    await axios.get(process.env.KONG_URL_REFERENTIEL_PRODUIT + '/products').then(function (products) {
        result = products.data;
    }).catch((err) => {
        result = err.data === undefined ? {"myerror": "My error : return data type /products is undefined: maybe table is empty"} : err.data;
    });
    
    return result
}

async function postProductsAssortmentPrices(products) {
    let result = '', code = 404
    await axios.post(process.env.KONG_URL_REFERENTIEL_PRODUIT + "/update-products", products).then((response) => {
        result = response.data, code = 200
    }, (error) => {
        result = error, code = 404
    });
    return [result, code]
}


module.exports.getProducts = getProducts
module.exports.postProductsAssortmentPrices = postProductsAssortmentPrices

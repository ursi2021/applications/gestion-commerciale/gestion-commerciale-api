const axios = require("axios");

async function getRestockingRequest() {
    let store_restocking_requests = '';
    await axios.get(process.env.KONG_URL_BACK_OFFICE_MAGASIN + '/store-restocking-request').then(function (restocking_requests) {
        store_restocking_requests = restocking_requests.data;
    }).catch((err) => {
        store_restocking_requests = err.data === undefined ? {"myerror": "My error : return data type /products is undefined: maybe table is empty"} : err.data;
    });

    return store_restocking_requests
}

module.exports.getRestockingRequest = getRestockingRequest
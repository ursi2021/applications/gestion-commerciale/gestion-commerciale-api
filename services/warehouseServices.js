const axios = require("axios");

async function getStockPosition() {
    let result = '';
    await axios.get(process.env.KONG_URL_ENTREPROTS + '/warehouse-stock-position').then(function (request) {
        result = request.data;
    }).catch((err) => {
        result = err.data === undefined ? {"myerror": "My error : return data type /position-stock-entrepots is undefined: maybe table is empty"} : err.data;
    });
    
    return result
}

module.exports.getStockPosition = getStockPosition
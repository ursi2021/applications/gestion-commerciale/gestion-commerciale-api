const axios = require("axios");
const sales_fake_json = require('../models/json/sales.json')

async function webSales() {
    let web_sales = []
    const webSales = await axios.get(process.env.KONG_URL_E_COMMERCE +'/web-sales').then(function (webSales) {
    	if ( webSales != null && webSales.length != null && webSales.length > 0 )
    	{
	        webSales.data.map(web_sale => {
	            const item = {
	                date: web_sale["date"],
	                acount: web_sale["account"],
	                "product-quantity-map": web_sale["product-quantity-map"],
	                "sell-type": "Web"
	            }
	            web_sales.push(item)
	        })
	    }
    })
    return web_sales
}

async function storeSales() {
    let store_sales = []

    await axios.get(process.env.KONG_URL_CAISSE +'/store-sales').then(function (storeSales) {
            const item = {
                date: storeSales.data.date,
                "product-quantity-map": storeSales.data["product-quantity-map"],
                "sell-type": "Store"
            }
            store_sales.push(item)
    })
    return store_sales
}

async function allSales() {
    let all_sales = []
    const webSales = await axios.get(process.env.KONG_URL_E_COMMERCE +'/web-sales').then(function (webSales) {
    if (webSales != null && webSales.length != null && webSales.length > 0 )
    	{
	        webSales.data.map(web_sale => {
	            const item = {
	                date: web_sale["date"],
	                acount: web_sale["account"],
	                "product-quantity-map": web_sale["product-quantity-map"],
	                "sell-type": "Web"
	            }
	            all_sales.push(item)
	        })
	    }
    })

    await axios.get(process.env.KONG_URL_CAISSE +'/store-sales').then(function (storeSales) {
            const item = {
                date: storeSales.data.date,
                "product-quantity-map": storeSales.data["product-quantity-map"],
                "sell-type": "Store"
            }
            all_sales.push(item)
    })
    return all_sales
}

async function clockSales(clockSales) {
    const all_sales = await allSales()
    return all_sales
}

module.exports.webSales = webSales;
module.exports.storeSales = storeSales;
module.exports.clockSales = clockSales;
module.exports.allSales = allSales;
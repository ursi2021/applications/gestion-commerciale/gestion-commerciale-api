let express = require('express');
let router = express.Router();
let app = express();

let templateController = require('../controller/template_controller');
let gestcoController = require('../controller/gestc_controller');
let helloController = require('../controller/hello_controller');
let assortmentsController = require('../controller/products_assortments_controller');
let salesController = require('../controller/sales_controller');
let purchasePriceController = require('../controller/purchase_price_controller');
let deliveryRequestController = require('../controller/delivery_requests_controller');
let providerOrdersController = require('../controller/provider_orders_controller');
let replenishmentController = require('../controller/replenishments_controller');
let clockController = require('../controller/clock_controller');

let ProductQuantity = require('../models/z_productOrdersModel');
const logger = require('../logs');

/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});*/

router.get('/', templateController.index)


/**@swagger
 * /users:
 *   get:
 *     tags:
 *       - Users
 *     summary: get users page
 *     responses:
 *       200:
 *         description: Return users html
 */
router.get('/users', templateController.getClientsPage);

/**@swagger
 * /api/user:
 *   get:
 *     tags:
 *       - Users
 *     summary: get all users
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of users
 *         schema:
 *           type: array
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/api/user', templateController.getClients);

/**@swagger
 * /api/user:
 *   post:
 *     summary: get all users
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: Return created user
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Failed to create data
 */
router.post('/api/user', templateController.postClients);

/**@swagger
 * /gestion-commerciale/hello:
 *   get:
 *     summary: get Hello World string
 *     tags:
 *       - Hello
 *     responses:
 *       200:
 *         description: Return hello world
 */
router.get('/gestion-commerciale/hello', helloController.hello)

/**@swagger
 * /gestion-commerciale/hello/all:
 *   get:
 *     summary: Show Hello World of all applications
 *     tags:
 *       - Hello
 *     responses:
 *       200:
 *         description: get hello world
 */
router.get('/gestion-commerciale/hello/all', helloController.helloAll)

router.get('/gestion-commerciale/clients', gestcoController.getClientsRefPage);

router.get('/gestion-commerciale/json/clients', gestcoController.getClientsRefData);

router.get('/gestion-commerciale/json/products', gestcoController.getProductsRefData);

router.get('/gestion-commerciale/delivery-requests', deliveryRequestController.getDeliveryRequests);

router.get('/gestion-commerciale/warehouse-stock-position', replenishmentController.warehouseStockPosition);

router.get('/gestion-commerciale/provider-orders', providerOrdersController.getProvideOrder);

router.post('/gestion-commerciale/test', gestcoController.getProductsTest);

/**@swagger
 * /gestion-commerciale/products/:assortments:
 *   get:
 *     tags:
 *       - Products
 *     summary: get specific Web / Store / Both assortments
 *     responses:
 *       200:
 *         description: Return new Web / Store / Both assortments
 */
router.get('/gestion-commerciale/products/:assortments', assortmentsController.productAssortments);
router.post('/gestion-commerciale/products/:assortments', assortmentsController.productAssortments);
router.get('/gestion-commerciale/products/:assortments', assortmentsController.productAssortments);

/**@swagger
 * /gestion-commerciale/products:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all products assortments json
 *     responses:
 *       200:
 *         description: Return all new products assortments
 */
router.get('/gestion-commerciale/products', assortmentsController.allProductAssortments);
router.post('/gestion-commerciale/products', assortmentsController.allProductAssortments);
router.get('/gestion-commerciale/products', assortmentsController.allProductAssortments);


/**@swagger
 * /gestion-commerciale/all-sales:
 *   get:
 *     tags:
 *       - Sales
 *     summary: get all web and stores sales
 *     responses:
 *       200:
 *         description: Return all web and stores sales
 */
router.get('/gestion-commerciale/all-sales', salesController.allSales);
/**@swagger
 * /gestion-commerciale/web-sales:
 *   get:
 *     tags:
 *       - Sales
 *     summary: get web sales from Ecommerce
 *     responses:
 *       200:
 *         description: Return web sales
 */
router.get('/gestion-commerciale/web-sales', salesController.webSales);
/**@swagger
 * /gestion-commerciale/store-sales:
 *   get:
 *     tags:
 *       - Sales
 *     summary: get stores sales from BOM
 *     responses:
 *       200:
 *         description: Return stores sales
 */
router.get('/gestion-commerciale/store-sales', salesController.storeSales);

router.post('/gestion-commerciale/clock-sales', salesController.clockSales);

router.post('/gestion-commerciale/clock-commands', replenishmentController.clockCommands);

/**@swagger
 * /gestion-commerciale/products-purchase-price:
 *   get:
 *     tags:
 *       - Price
 *     summary: return buy_price of products json
 *     responses:
 *       200:
 *         description: Return buy_price of products json
 */
router.post('/gestion-commerciale/products-purchase-price', purchasePriceController.purchasePrice);

module.exports = router;

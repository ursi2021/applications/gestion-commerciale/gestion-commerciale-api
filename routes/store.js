let express = require('express');
let router = express.Router();
let app = express();

let gestcoController = require('../controller/gestc_controller')
let productController = require('../controller/products_assortments_controller')
let replenishmnentController = require('../controller/replenishments_controller')

router.get('/page1', gestcoController.gestco_page1)
router.get('/page2', gestcoController.gestco_page2)
router.get('/', gestcoController.index)

router.post('/get-products', productController.productsTraitment);

/**@swagger
 * /gestion-commerciale/replenisment:
 *   get:
 *     tags:
 *       - Replenishments
 *     summary: Get all providers requests from back office and warehouse
 *     responses:
 *       200:
 *         description: Return all replenishments requests
 */
router.get('/replenisment', replenishmnentController.replenisment);

/**@swagger
 * /gestion-commerciale/calcul-replenisment-back-office:
 *   get:
 *     tags:
 *       - Replenishments
 *     summary: Get reassort requests from BOM + check warehouse stock products quantity + launch delivery requests to providers
 *     responses:
 *       200:
 *         description: Send back office replenishments requests to providers
 */
router.get('/calcul-replenisment-back-office', replenishmnentController.caluclReplenisment);

/**@swagger
 * /gestion-commerciale/calcul-replenisment-warehouse:
 *   get:
 *     tags:
 *       - Replenishments
 *     summary: Get reappro requests from warehouse + launch delivery requests to providers
 *     responses:
 *       200:
 *         description: Send warehouse replenishments requests to providers
 */
router.get('/calcul-replenisment-warehouse', replenishmnentController.commandWarehouseReapro);

router.get('/store-restocking-request', replenishmnentController.storeRestockingRequest);


module.exports = router;
